# ansible-apport  
This module allows activating or deactivating apport on an ubuntu installation  

# Supported variables  
This module support only the following variable:  
* apport_enabled: <true|false> (default true)


